import React from 'react';
import './App.less';
import TodoList from "./components/TodoList";

class App extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {showAble: true};
        this.changeListState = this.changeListState.bind(this);
        this.refreshPage = this.refreshPage.bind(this);
    }

    changeListState() {
        this.setState({showAble: !this.state.showAble});
    }

    render() {
        return (
            <div className='App'>
            <section className={'buttonComponent'}>
                <button className={'showAbleButton'} onClick={this.changeListState}>
                    {this.state.showAble ? 'show' : 'hide'}</button>
                <button className={'refresh'} onClick={this.refreshPage}>refresh</button>
            </section>
                {this.state.showAble ? null : <TodoList show={this.state.showAble}/>}
            </div>
        )
    }

    componentDidMount() {
        console.log(new Date().toLocaleTimeString() + 'App componentDidMount');
    }

    componentWillUnmount() {
        console.log(new Date().toLocaleTimeString() + 'App componentWillUnmount');
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log(new Date().toLocaleTimeString() + 'App componentDidUpdate')
    }

    refreshPage() {
        this.setState({showAble: true});
        window.location.reload();
    }

}

export default App;